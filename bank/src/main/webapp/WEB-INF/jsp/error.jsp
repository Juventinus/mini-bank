<%@ taglib prefix="form" uri="http://www.springframework.org/tags/form" %>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@ page language="java" contentType="text/html; charset=ISO-8859-1"
    pageEncoding="ISO-8859-1"%>
<!DOCTYPE html>
<html>
<%@ include file="home.jsp" %>
<body>
<h2>Application Error, please contact support.</h2>
<p>Message <span class="font-weight-bold">${errorMessage}</span></p>

<%@ include file="footer.jsp" %>
</body>
</html>

