<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>
<%@ page language="java" contentType="text/html; charset=ISO-8859-1"
	pageEncoding="ISO-8859-1"%>
<!doctype html>
<html lang="en">
<%@ include file="home.jsp"%>
<body>
	<%@ include file="navbar.jsp"%>
	<div class="wrapper">
		<%@ include file="sidebar.jsp"%>
		<!-- Page Content  -->
		<div id="content">
			<div class="container-fluid">
				<!-- add out the html table -->
				<div class="container">
					<h3>Account List</h3>
					<table id="example"
						class="table-light table-striped table-bordered"
						style="width: 100%">
						<thead>
							<tr>
								<th class="text-center">Customer Name</th>
								<th class="text-center">Account Number</th>
								<th class="text-center">Amount</th>
								<th class="text-center">Status</th>
								<th class="text-center">Action</th>
							</tr>
						</thead>
						<tbody>
							<!-- loop over and print our customers -->
							<c:forEach var="tempCustomer" items="${accounts}">

								<tr>
									<td>${tempCustomer.customer.name}</td>
									<td>${tempCustomer.acct_num}</td>
									<td>${tempCustomer.amount}</td>
									<td>${tempCustomer.status}</td>
									<td>
										<!-- display the update link --> <a
										href="/Account/update?accountId=${tempCustomer.id}"
										class="mr-2">Update</a> <a
										href="/Account/delete?accountId=${tempCustomer.id}"
										onclick="if(!(confirm('Are you sure you want to delete this customer?'))) return false">Delete</a>
									</td>
								</tr>
							</c:forEach>
						</tbody>
					</table>
					<!-- Add new button: Add Column -->
				</div>
				<div class="mt-3">
					<a href="/Account/show" class="btn btn-default">Add Account</a>
				</div>

			</div>
		</div>
	</div>
	<%@ include file="footer.jsp"%>
</body>
</html>
