<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>
<%@ page language="java" contentType="text/html; charset=ISO-8859-1"
	pageEncoding="ISO-8859-1"%>
<!doctype html>
<html lang="en">
<%@ include file="home.jsp"%>
<body>
	<%@ include file="navbar.jsp"%>
	<div class="wrapper">
		<%@ include file="sidebar.jsp"%>
		<!-- Page Content  -->
		<div id="content">
			<div class="container-fluid">
				<!-- add out the html table -->
				
				
				<div class="container">
					<h3>Transaction List</h3>
					<table id="example"
						class="table-light table-striped table-bordered"
						style="width: 100%">
						<thead>
							<tr>
								<th class="text-center">AccountNumber</th>
								<th class="text-center">Amount</th>
								<th class="text-center">TransactionType</th>
								<th class="text-center">CreatedDate</th>
							</tr>
						</thead>
						<tbody>
							<c:forEach var="tempTrans" items="${transaction}">

								<tr>
									<td>${tempTrans.account}</td>
									<td>${tempTrans.amount}</td>
									<td>${tempTrans.transType}</td>
									<td>${tempTrans.createdDate}</td>

								</tr>
							</c:forEach>
						</tbody>
					</table>
					<!-- Add new button: Add Column -->
				</div>
			</div>
		</div>
	</div>
	<%@ include file="footer.jsp"%>
</body>
</html>
