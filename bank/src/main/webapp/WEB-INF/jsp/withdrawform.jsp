<%@ taglib prefix="form" uri="http://www.springframework.org/tags/form"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>
<%@ page language="java" contentType="text/html; charset=ISO-8859-1"
	pageEncoding="ISO-8859-1"%>
<!doctype html>
<html lang="en">
<%@ include file="home.jsp"%>
<body>
	<%@ include file="navbar.jsp"%>
	<div class="wrapper">
		<%@ include file="sidebar.jsp"%>
		<!-- Page Content  -->
		<div id="content">
			<div class="container-fluid">
				<div class="container">
					<h3>Withdrawal Form</h3>
					<form:form action="saveWithDraw" modelAttribute="withdraw"
						method="POST">

						<!-- need to associate this data with a customer id -->
						<form:hidden path="id" />
						<label>AccountNumber:</label>
					&nbsp; &nbsp; 
					<form:input path="account" /><br>
						<label>Amount:</label>
					&nbsp; &nbsp; &nbsp; &nbsp;
					<form:input path="amount" /><br>
						<label></label>
						<input type="submit" value="Save" class="save" />
					</form:form>
				</div>
			</div>
		</div>
	</div>
	<%@ include file="footer.jsp"%>
</body>
</html>

