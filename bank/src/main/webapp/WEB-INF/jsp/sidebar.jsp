<!-- Sidebar  -->
<nav id="sidebar">
	<div class="sidebar-header">
		<h3>CRM</h3>

	</div>

	<ul class="list-unstyled components">
		<li class="active"><a href="#acctSubmenu" data-toggle="collapse"
			aria-expanded="false" class="dropdown-toggle"> <i
				class="fas fa-bank"></i> Account
		</a>
			<ul class="collapse list-unstyled" id="acctSubmenu">
				<li><a href="/Account/show">New Account</a></li>
				<li><a href="/Account/list">Account List</a></li>
			</ul></li>
		<li><a href="/Transaction/deposit"> <i class="fa fa-google-wallet"></i>
				Deposit
		</a></li>
		<li><a href="/Transaction/withdraw"> <i
				class="fa fa-credit-card"></i> WithDraw
		</a></li>
		<li><a href="/Customer/customerForm"> <i class="fa fa-group"></i> Customers
		</a></li>
		<li><a href="/Transaction/listT"> <i class="fa fa-history"></i> Transactions
		</a></li>
	</ul>

	<ul class="list-unstyled CTAs">
		<li></li>
		<li></li>
	</ul>
</nav>
