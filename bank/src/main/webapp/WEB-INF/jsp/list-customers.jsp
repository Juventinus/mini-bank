<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>
<%@ page language="java" contentType="text/html; charset=ISO-8859-1"
	pageEncoding="ISO-8859-1"%>
<!doctype html>
<html lang="en">
<%@ include file="home.jsp"%>
<body>
	<%@ include file="navbar.jsp"%>
	<div class="wrapper">
		<%@ include file="sidebar.jsp"%>
		<!-- Page Content  -->
		<div id="content">
			<div class="container-fluid">
				<!-- add out the html table -->
				<div class="container">
					<h3>Customer List</h3>
					<table id="example"
						class="table-light table-striped table-bordered"
						style="width: 100%">
						<thead>
							<tr>
								<th class="text-center">First Name</th>
								<th class="text-center">PhoneNumber</th>
								<th class="text-center">Email</th>
								<th class="text-center">Action</th>
							</tr>
						</thead>
						<tbody>
							<c:forEach var="tempCustomer" items="${customer}">

								<tr>
									<td>${tempCustomer.name}</td>
									<td>${tempCustomer.phoneNum}</td>
									<td>${tempCustomer.email}</td>
									<td>
										<!-- display the update link --> <a
										href="/Customer/update?customerId=${tempCustomer.id}"
										class="mr-2">Update</a> <a
										href="/Customer/delete?customerId=${tempCustomer.id}"
										onclick="if(!(confirm('Are you sure you want to delete this customer?'))) return false">Delete</a>
									</td>
								</tr>
							</c:forEach>
						</tbody>
					</table>
					<!-- Add new button: Add Column -->
				</div>
				<div class="mt-3">
					<a href="/Customer/customerForm" class="btn btn-default">Add
						Customer</a>
				</div>

			</div>
		</div>
	</div>
	<%@ include file="footer.jsp"%>
</body>
</html>
