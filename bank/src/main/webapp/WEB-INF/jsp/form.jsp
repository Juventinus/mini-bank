<%@ taglib prefix="form" uri="http://www.springframework.org/tags/form"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>
<%@ page language="java" contentType="text/html; charset=ISO-8859-1"
	pageEncoding="ISO-8859-1"%>
<!doctype html>
<html lang="en">
<%@ include file="home.jsp"%>
<body>
	<%@ include file="navbar.jsp"%>
	<div class="wrapper">
		<%@ include file="sidebar.jsp"%>
		<!-- Page Content  -->
		<div id="content">
			<div class="container-fluid">
				<div class="container">
					<h3>Account Form</h3>
					<form:form action="saveAccount" modelAttribute="account"
						method="POST">

						<!-- need to associate this data with a customer id -->
						<form:hidden path="id" />
						<div class="form-group">
							<!--  -->
							<label>Customer Name:</label> &nbsp; &nbsp; &nbsp; &nbsp;
							<form:select path="customerId"
								class="browser-default custom-select" items="${customerList}"
								itemLabel="name">
								<form:option value="0" label="Select Customer" />
							</form:select>
						</div>
						<div class="form-group">
							<label>Account Number:</label> &nbsp; &nbsp; &nbsp; &nbsp;
							<form:input path="acct_num" />
						</div>
						<div class="form-group">
							<label>Amount:</label> &nbsp; &nbsp; &nbsp; &nbsp;&nbsp; &nbsp;
							&nbsp; &nbsp;
							<form:input path="amount" />
						</div>
						<div class="form-group">
							<label>Limit:</label> &nbsp; &nbsp; &nbsp; &nbsp;&nbsp; &nbsp;
							&nbsp; &nbsp;
							<form:input path="limit" />
						</div>
						<div class="form-group">
							<label>Account Type:</label> &nbsp;
							<form:select path="type" value="${account.type}">
								<option value="Savings">Savings</option>
								<option value="current">Current</option>
							</form:select>
						</div>
						<div class="form-group">
							<label>Status:</label> &nbsp; &nbsp; &nbsp; &nbsp;
							<form:select path="status" value="${account.status}">
								<option value="Active">Active</option>
								<option value="Inactive">InActive</option>
							</form:select>
						</div>
						<label></label>
						<input type="submit" value="Save" class="save" />


					</form:form>
				</div>
			</div>
		</div>
	</div>
	<%@ include file="footer.jsp"%>
</body>
</html>

