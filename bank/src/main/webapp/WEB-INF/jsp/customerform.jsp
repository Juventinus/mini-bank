<%@ taglib prefix="form" uri="http://www.springframework.org/tags/form"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>
<%@ page language="java" contentType="text/html; charset=ISO-8859-1"
	pageEncoding="ISO-8859-1"%>
<!doctype html>
<html lang="en">
<%@ include file="home.jsp"%>
<body>
	<%@ include file="navbar.jsp"%>
	<div class="wrapper">
		<%@ include file="sidebar.jsp"%>
		<!-- Page Content  -->
		<div id="content">
			<div class="container-fluid">
				<div class="container">
					<h3>Customer Form</h3>
					<form:form action="saveCustomer" modelAttribute="customer"
						method="POST">
						<form:hidden path="id" />
						<label>First Name:</label>
					&nbsp; &nbsp; &nbsp; &nbsp;
					<form:input path="name" /><br>
						<label>PhoneNumber:</label>
					&nbsp; &nbsp;
					<form:input path="phoneNum" /><br>
						<label>Email:</label>
					&nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp;
					<form:input path="email" /><br>
						<input type="submit" value="Save" class="save" />
					</form:form>
					<div class="card-footer">
						<p>
							<a href="/Customer/list">Back to List</a>
						</p>
					</div>
				</div>
			</div>
		</div>
	</div>
	<%@ include file="footer.jsp"%>
</body>
</html>

