package com.mini.bank.controller;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;

import com.mini.bank.entity.Account;
import com.mini.bank.entity.Customer;
import com.mini.bank.service.AccountService;
import com.mini.bank.service.CustomerService;

@Controller
@RequestMapping("/Account")
public class AccountController {

	@Autowired
	AccountService accountService;

	@Autowired
	CustomerService customerService;

	@GetMapping("/show")
	public String showFormForAdd(Model model) {

		// create model attribute to bind form data
		Account account = new Account();
		List<Customer> cus = customerService.getCustomers();
		System.out.println(cus);
		model.addAttribute("customerList", cus);
		model.addAttribute("account", account);
		return "form";
	}

	@PostMapping("/saveAccount")
	public String saveAccount(@ModelAttribute("account") Account account) {
		// save the account using our service
		accountService.saveAccount(account);

		return "redirect:/Account/list";
	}

	@GetMapping("/list")
	public String listAccounts(Model model) {
		// get accounts from the service
		List<Account> account = accountService.getAccounts();
		// add the account to the model
		model.addAttribute("accounts", account);

		return "list-account";
	}
	
	@GetMapping("/update")
	public String updateAccount(@RequestParam("accountId") Long id, Model model) {
		Account account = accountService.getAccount(id);
		account.setCustomerId(account.getCustomer().getId());
		List<Customer> cus = customerService.getCustomers();
		model.addAttribute("customerList", cus);
		model.addAttribute("account", account);
		return "form";
	}

	@GetMapping("/delete")
	public String deleteAccount(@RequestParam("accountId") Account id) {

		accountService.deleteAccount(id);
		return "redirect:/Account/list";
	}

}
