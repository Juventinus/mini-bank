package com.mini.bank.controller;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestMapping;

import com.mini.bank.entity.Transaction;
import com.mini.bank.entity.Transactional;
import com.mini.bank.service.TransactionService;

@Controller
@RequestMapping("/Transaction")
public class Transactioncontroller {

	@Autowired
	TransactionService transactionService;

	@GetMapping("/withdraw")
	public String showFormForAdd(Model model) {
		// create model attribute to bind form data
		Transaction trans = new Transaction();
		model.addAttribute("withdraw", trans);
		return "withdrawform";
	}

	@PostMapping("/saveWithDraw")
	public String saveWithDraw(@ModelAttribute("withdraw") Transaction transaction) {
		// save the transaction using our service
		transaction.setTransType(Transactional.WITHDRAW);
		transactionService.create(transaction);
		return "redirect:/Transaction/listT";
	}

	@GetMapping("/deposit")
	public String showForm(Model model) {
		Transaction transaction = new Transaction();
		model.addAttribute("deposit", transaction);
		return "depositform";
	}

	@PostMapping("/saveDeposit")
	public String saveTransaction(@ModelAttribute("deposit") Transaction transaction) {
		transaction.setTransType(Transactional.DEPOSIT);
		transactionService.create(transaction);
		return "redirect:/Transaction/listT";
	}

	@GetMapping("/listT")
	public String listTransctions(Model model) {
		List<Transaction> transaction = transactionService.getTransactions();
		model.addAttribute("transaction", transaction);
		return "listT";
	}

}
