package com.mini.bank.controller;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.mail.MailException;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import com.mini.bank.entity.Customer;
import com.mini.bank.service.EmailService;

@RestController
@RequestMapping("/email")
public class EmailController {

	@Autowired
	private EmailService notificationService;
	
	
	@GetMapping("/sendmail") 
	public String send(@RequestParam String email) {
		try {
			Customer customer = new Customer();
			customer.setEmail(email);
			notificationService.sendEmail(customer);
		} catch (MailException mailException) {
			System.out.println(mailException);
		}
		return "Congratulations! Your mail has been send to the user.";
	}

}