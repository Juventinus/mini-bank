package com.mini.bank.controller;

import javax.servlet.http.HttpServletRequest;

import org.springframework.validation.BindException;
import org.springframework.web.bind.annotation.ControllerAdvice;
import org.springframework.web.bind.annotation.ExceptionHandler;
import org.springframework.web.servlet.ModelAndView;

@ControllerAdvice
public class ErrorController {

	
	@ExceptionHandler(Exception.class)
	public ModelAndView handleSQLException(HttpServletRequest request, Exception ex){
		ModelAndView modelAndView = new ModelAndView();
		modelAndView.addObject("errorMessage", ex.getMessage());
		modelAndView.setViewName("error");
		return modelAndView;
	}
	
	@ExceptionHandler(BindException.class)
	public ModelAndView handleSQLException(HttpServletRequest request, BindException ex){
		ModelAndView modelAndView = new ModelAndView();
		modelAndView.addObject("errorMessage", ex.getMessage());
		modelAndView.setViewName("error");
		return modelAndView;
	}
}
