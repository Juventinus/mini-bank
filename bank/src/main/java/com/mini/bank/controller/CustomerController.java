package com.mini.bank.controller;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;

import com.mini.bank.entity.Customer;
import com.mini.bank.service.CustomerService;

@Controller
@RequestMapping("/Customer")
public class CustomerController {

	@Autowired
	CustomerService customerService;

	@GetMapping("/customerForm")
	public String showFormForAdd(Model model) {

		// create model attribute to bind form data
		Customer customer = new Customer();

		model.addAttribute("customer", customer);
		return "customerform";
	}

	@PostMapping("/saveCustomer")
	public String saveCustomer(@ModelAttribute("customer") Customer customer) {
		// save the customer using our service
		customerService.saveCustomer(customer);

		return "redirect:/Customer/list";
	}

	@GetMapping("/list")
	public String listCustomers(Model model) {

		// get customers from the service
		List<Customer> customer = customerService.getCustomers();

		// add the customers to the model
		model.addAttribute("customer", customer);

		return "list-customers";
	}
	
	@GetMapping("/update")
	public String updateCustomer(@RequestParam("customerId") Long id, Model model) {
     	Customer customer = customerService.getCustomer(id);
		
		model.addAttribute("customer", customer);
		return "customerform";
	}
	@GetMapping("/delete")
	public String deleteAccount(@RequestParam("customerId") Customer id) {

		customerService.deleteCustomer(id);
		return "redirect:/Customer/list";
	}


}
