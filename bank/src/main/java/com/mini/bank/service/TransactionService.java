package com.mini.bank.service;

import java.util.List;

import com.mini.bank.entity.Transaction;

public interface TransactionService {

	public void create(Transaction transaction);

	List<Transaction> getTransactions();

	void sendEmail(Transaction transaction);

}
