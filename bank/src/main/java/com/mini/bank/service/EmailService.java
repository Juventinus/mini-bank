package com.mini.bank.service;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.mail.MailException;
import org.springframework.mail.SimpleMailMessage;
import org.springframework.mail.javamail.JavaMailSender;
import org.springframework.stereotype.Service;

import com.mini.bank.entity.Message;
import com.mini.bank.repository.MessageRepository;

@Service
public class EmailService {

	@Autowired
	CustomerService customerService;
	@Autowired
	AccountService accountService;

	@Autowired
	MessageRepository messageRepository;
	
	private JavaMailSender javaMailSender;

	@Autowired
	public EmailService(JavaMailSender javaMailSender) {
		this.javaMailSender = javaMailSender;
	}

	public void sendEmail(Message message) {
		SimpleMailMessage mail = new SimpleMailMessage();
		mail.setTo(message.getTo());
		mail.setSubject(message.getSubject());
		mail.setText(message.getContent());
		messageRepository.save(message);
		try {
			javaMailSender.send(mail);
		} catch (Exception e) {
			System.out.println("<<<<<<< Error Sending Mail >>>>>>");
		}
	}

}