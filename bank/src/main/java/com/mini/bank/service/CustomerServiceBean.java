package com.mini.bank.service;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.mini.bank.entity.Customer;
import com.mini.bank.repository.CustomerRepository;
@Service
public class CustomerServiceBean implements CustomerService {

	@Autowired
	CustomerRepository customerRep;
	
	@Override
	public void saveCustomer(Customer theCustomer) {
		customerRep.save(theCustomer);
	}

	@Override
	public List<Customer> getCustomers() {
		return customerRep.findAll();
	}

	@Override
	public Customer getCustomer(Long id) {
		return customerRep.getOne(id);
	}

	@Override
	public void deleteCustomer(Customer id) {
		
		customerRep.delete(id);

	}

}
