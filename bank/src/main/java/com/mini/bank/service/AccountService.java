package com.mini.bank.service;

import java.util.List;

import com.mini.bank.entity.Account;
import com.mini.bank.entity.Customer;

public interface AccountService {
	 
	public void saveAccount(Account theAccount);

	public List<Account> getAccounts();

	public Account getAccount(Long id);
     
	public void deleteAccount(Account id);
	
	public Customer getcustomer(Long id);

	public List<Customer> getcustomers(Long id);
	
	public Account findByAccountNumber(String num);
	
	public Account findByAmount(Double amount);

}
