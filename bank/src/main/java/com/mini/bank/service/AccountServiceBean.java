package com.mini.bank.service;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.mini.bank.entity.Account;
import com.mini.bank.entity.Customer;
import com.mini.bank.repository.AccountRep;
import com.mini.bank.repository.CustomerRepository;

@Service
public class AccountServiceBean implements AccountService{
	
	@Autowired
	AccountRep accountRep;
	@Autowired
	CustomerService customerService;

	@Override
	public void saveAccount(Account theAccount) {
		Customer customer = customerService.getCustomer(theAccount.getCustomerId());
		if (customer == null) {
			throw new IllegalArgumentException("Customer does not exist");
		}
		theAccount.setCustomer(customer);
		accountRep.save(theAccount);
	}

	@Override
	public List<Account> getAccounts() {
		
		return accountRep.findAll();
	}

	@Override
	public Account getAccount(Long id) {
		
		return accountRep.getOne(id);
	}

	@Override
	public void deleteAccount(Account id) {
		
		accountRep.delete(id);
		
	}

	@Override
	public Customer getcustomer(Long id) {
		
		return accountRep.findCustomer(id);
	}

	@SuppressWarnings("unchecked")
	@Override
	public List<Customer> getcustomers(Long id) {
		
		return (List<Customer>) accountRep.findCustomer(id);
	}

	@Override
	public Account findByAccountNumber(String num) {
		
		return accountRep.findAccount(num);
	}

	@Override
	public Account findByAmount(Double amount) {
		
		return accountRep.findByAmount(amount);
	}

	

	

	
	
	

}
