package com.mini.bank.service;

import java.time.Instant;
import java.util.Date;
import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.util.ObjectUtils;
import org.springframework.util.StringUtils;

import com.mini.bank.entity.Account;
import com.mini.bank.entity.Message;
import com.mini.bank.entity.Transaction;
import com.mini.bank.entity.Transactional;
import com.mini.bank.repository.AccountRep;
import com.mini.bank.repository.TransactionRepository;

@Service
public class TransactionServiceBean implements TransactionService {

	@Autowired
	TransactionRepository transactionRepository;

	@Autowired
	AccountService accountService;
	@Autowired
	AccountRep accountRep;
	@Autowired
	EmailService emailService;

	@Override
	public void create(Transaction transaction) {
		Account account = validateTransaction(transaction);
		Transaction saved = transactionRepository.save(transaction);
		accountRep.save(account);
		saved.setCreatedDate(Date.from(Instant.now()));
		sendEmail(saved);
	}

	@Override
	public List<Transaction> getTransactions() {
		return transactionRepository.findAll();
	}

	private Account validateTransaction(Transaction transaction) {
		Account acct = accountService.findByAccountNumber(transaction.getAccount());
		if (!ObjectUtils.isEmpty(acct)) {
			if (Transactional.WITHDRAW.equals(transaction.getTransType())) {

				if (acct.getAmount() == 0) {
					throw new IllegalArgumentException("Insuffient Fund");
				}
				if (transaction.getAmount() <= acct.getLimit()) {
					Double bal = acct.getAmount() - transaction.getAmount();
					acct.setAmount(bal);
				} else {
					throw new IllegalArgumentException("You have Exceed your Limit");
				}

			} else {
				Double bal = transaction.getAmount() + acct.getAmount();
				acct.setAmount(bal);
			}
		} else {
			throw new IllegalArgumentException("Account number " + transaction.getAccount() + " does not exist.");
		}
		return acct;
	}

	@Override
	public void sendEmail(Transaction transaction) {
		Account acct = accountService.findByAccountNumber(transaction.getAccount());
		if (!ObjectUtils.isEmpty(acct.getCustomer())) {
			if (!StringUtils.isEmpty(acct.getCustomer().getEmail())) {
				Message message = new Message();
				message.setTo(acct.getCustomer().getEmail());
				if (Transactional.WITHDRAW.equals(transaction.getTransType())) {
					message.setSubject("WithDrawal Receipt");
					message.setContent(transaction.getAmount() + "  " + "Was withdrawn from your mini-bank account"
							+ "  " + acct.getAcct_num() + "  " + "and your new balance is" + "  " + acct.getAmount());
				} else {
					message.setSubject("Deposite Receipt");
					message.setContent(transaction.getAmount() + " " + "Was deposited into your mini-bank account" + " "
							+ acct.getAcct_num() + " " + "and your new balance is" + "   " + acct.getAmount());
				}
				emailService.sendEmail(message);
			}
		}
	}

}
