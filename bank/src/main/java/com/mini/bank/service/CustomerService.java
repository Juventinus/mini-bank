package com.mini.bank.service;

import java.util.List;

import com.mini.bank.entity.Customer;

public interface CustomerService {
	
	public void saveCustomer(Customer theCustomer);

	public List<Customer> getCustomers();

	public Customer getCustomer(Long id);
     
	public void deleteCustomer(Customer id);

}
