package com.mini.bank.repository;

import org.springframework.data.jpa.repository.JpaRepository;

import com.mini.bank.entity.Message;

public interface MessageRepository extends JpaRepository<Message, Long> {

}