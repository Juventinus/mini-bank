package com.mini.bank.repository;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.stereotype.Repository;

import com.mini.bank.entity.Account;
import com.mini.bank.entity.Customer;

@Repository
public interface AccountRep extends JpaRepository<Account, Long> {

	@Query("SELECT a FROM Account a WHERE a.id = ?1")
	Account findAccount(Long id);

	@Query("SELECT b FROM Customer b WHERE b.name = ?1")
	Customer findCustomer(Long id);

	@Query("SELECT c FROM Account c WHERE c.acct_num= ?1")
	Account findAccount(String acct_num);
	
	Account findByAmount(Double amount);
}
