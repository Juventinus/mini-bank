package com.mini.bank.entity;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.SequenceGenerator;
import javax.persistence.Table;

import lombok.Data;

@Data
@Entity
@Table(name = "EMAIL_MSGS")
public class Message {

	@Id
	@GeneratedValue(strategy = GenerationType.SEQUENCE, generator = "SEQUENCE1")
	@SequenceGenerator(name = "SEQUENCE1", sequenceName = "SEQUENCE1", allocationSize = 1)
	private Long id;
	@Column(name = "MSG_TO", nullable = true)
	private String to;
	@Column(name = "MSG_CONTENT", nullable = true)
	private String content;
	@Column(name = "MSG_SUBJECT", nullable = true)
	private String subject;
}
