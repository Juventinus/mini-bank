package com.mini.bank.entity;

import java.util.Date;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.EnumType;
import javax.persistence.Enumerated;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.SequenceGenerator;
import javax.persistence.Table;

import lombok.Data;

@Data
@Entity
@Table(name = "TRANSACTION")
public class Transaction {
	@Id
	@GeneratedValue(strategy = GenerationType.SEQUENCE, generator = "SEQUENCE1")
	@SequenceGenerator(name = "SEQUENCE1", sequenceName = "SEQUENCE1", allocationSize = 1)
	private Long id;

	@Column(name = "ACCOUNT_NUM")
	private String account;

	@Column(name = "AMOUNT")
	private Double amount;

	@Column(name = "TYPE")
	@Enumerated(EnumType.STRING)
	public Transactional transType;

	
    @Column(name = "created_date")
    private Date createdDate;
	
}
