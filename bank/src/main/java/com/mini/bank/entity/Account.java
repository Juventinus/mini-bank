package com.mini.bank.entity;

import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.OneToOne;
import javax.persistence.SequenceGenerator;
import javax.persistence.Table;
import javax.persistence.Transient;

import lombok.Data;

@Data
@Entity
@Table(name="Bank_Accounts")
public class Account {
	
	@Id
	@GeneratedValue(strategy=GenerationType.SEQUENCE, generator="SEQUENCE1")
	@SequenceGenerator(name = "SEQUENCE1", sequenceName = "SEQUENCE1", allocationSize = 1)
	private Long id;

	@JoinColumn(name = "CUSTOMER_ID", nullable = true)
	@OneToOne(cascade= {CascadeType.ALL })
	private Customer customer;
	
	@Column(name="ACCT_NUM",nullable=true)
	private String acct_num;
	
	
	@Column(name="AMOUNT",nullable=true)
	private Double amount;
	
	@Column(name="LIMIT",nullable=true)
	private Double limit;
	
	@Column(name="TYPE",nullable=true)
	private String type;
	
	@Column(name="STATUS",nullable=true)
	private String status;
	
	@Transient
	private Long customerId;

}
