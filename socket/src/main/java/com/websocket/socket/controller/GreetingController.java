package com.websocket.socket.controller;

import org.springframework.messaging.handler.annotation.MessageMapping;
import org.springframework.messaging.handler.annotation.Payload;
import org.springframework.messaging.handler.annotation.SendTo;
import org.springframework.messaging.simp.SimpMessageHeaderAccessor;
import org.springframework.stereotype.Controller;
import com.websocket.socket.model.Greeting;
import com.websocket.socket.model.HelloMessage;

@Controller
public class GreetingController {

	@MessageMapping("/hello.greeting")
	@SendTo("/topic/greetings")
	public HelloMessage greeting(@Payload HelloMessage message) {
		// Thread.sleep(1000); // simulated delay
		return message;
	}

	@MessageMapping("/hello.addUser")
	@SendTo("/topic/greetings")
	public HelloMessage addUser(@Payload HelloMessage chatMessage, SimpMessageHeaderAccessor headerAccessor) {
		// Add username in web socket session
		headerAccessor.getSessionAttributes().put("username", chatMessage.getSender());
		return chatMessage;
	}
}
